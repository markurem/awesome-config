# Awesome WM config

Customized awesome wm config.

## Installation

Requirements: ```awesome v4.0```.

```
cd ~
git clone git@bitbucket.org:markurem/awesome-config.git .config/awesome
cd ./config/awesome
git submodule init && git submodule update
sudo apt install $(cat requirements.txt)
```

## Open TODOs

* stuff and things