#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run nm-applet
run redshift-gtk
run setxkbmap -layout "us"
run numlockx on
